function Blockfield_GUI()
{

	%screenWidth = getWord($pref::Video::Resolution,0);
	%screenHeight = getWord($pref::Video::Resolution,1);
	
	%scorew = new GuiSwatchCtrl(BlockFieldHUD_ScoreWindow)
	{
		profile = "GuiDefaultProfile";
		horizSizing = "right";
		vertSizing = "bottom";
		position = (%screenWidth - 256) SPC (%screenHeight - 66);
		extent = "240 50";
		minExtent = "8 2";
		enabled = "1";
		visible = "1";
		clipToParent = "1";
		color = "0 0 0 200";
	};
	PlayGui.add(%scorew);
	
	%score = new GuiMLTextCtrl(BlockfieldHUD_Score)
	{
		profile = "GuiMLTextProfile";
		horizSizing = "right";
		vertSizing = "bottom";
		position = "0 0";
		extent = "234 48";
		minExtent = "8 2";
		enabled = "1";
		visible = "1";
		clipToParent = "1";
		lineSpacing = "2";
		allowColorChars = "0";
		maxChars = "-1";
		text = "<just:right><font:Impact:48><color:ffffff>N/A";
		maxBitmapHeight = "-1";
		selectable = "1";
		autoResize = "1";
	};
	BlockFieldHUD_ScoreWindow.add(%score);
	
	%timew = new GuiSwatchCtrl(BlockfieldHUD_TimeWindow)
	{
		profile = "GuiDefaultProfile";
		horizSizing = "right";
		vertSizing = "bottom";
		position = (%screenWidth - 316) SPC (%screenHeight - 66);
		extent = "60 25";
		minExtent = "8 2";
		enabled = "1";
		visible = "1";
		clipToParent = "1";
		color = "0 0 0 128";
	};
	PlayGui.add(%timew);

	%time = new GuiMLTextCtrl(BlockfieldHUD_Time)
	{
		profile = "GuiMLTextProfile";
		horizSizing = "right";
		vertSizing = "bottom";
		position = "0 3";
		extent = "60 20";
		minExtent = "8 2";
		enabled = "1";
		visible = "1";
		clipToParent = "1";
		lineSpacing = "2";
		allowColorChars = "0";
		maxChars = "-1";
		text = "<just:center><font:Impact:20><color:ffffff>N/A";
		maxBitmapHeight = "-1";
		selectable = "1";
		autoResize = "1";
	};
	BlockfieldHUD_TimeWindow.add(%time);
	
	
}

function Blockfield_Score()
{

	cancel($Blockfield_AddScore);
	
	//this will probably end up being dynamic depending on the level or maybe time, we'll see.
	//heh, maybe even a little getRandom too?
	$Blockfield::Score += 42;
	BlockfieldHUD_Score.setText("<just:right><font:Impact:48><color:ffffff>" @ $Blockfield::Score);
	
	$Blockfield_AddScore = schedule(10,0,Blockfield_Score);
	
}

function Blockfield_Time()
{

	cancel($Blockfield_Time);
	
	//thinking this will be what will increase levels rather than say, bricks avoided.
	$Blockfield::Time += 1;
	%time = getTimeString($Blockfield::Time);
	BlockfieldHUD_Time.setText("<just:center><font:Impact:20><color:ffffff>" @ %time);
	
	$Blockfield_Time = schedule(1000,0,Blockfield_Time);
	
	if($Blockfield::Time == 3) { clientcmdBottomPrint("",1); }
	
}

function clientcmdBlockfield_InitSys()
{

	$Blockfield::Score = 0;
	$Blockfield::Time = 0;

	Blockfield_GUI();
	
}

function clientcmdBlockfield_Start()
{

	Blockfield_Score();
	Blockfield_Time();

}

function Blockfield_onLeaveClient()
{

	$Blockfield::Score = 0;
	$Blockfield::Time = 0;
	
	for(%i=0;%i<4;%i++)
	{
		
		cancel($Blockfield_AddScore);
		cancel($Blockfield_Time);
		
	}
	
	BlockFieldHUD_ScoreWindow.delete();
	BlockfieldHUD_TimeWindow.delete();
	
}