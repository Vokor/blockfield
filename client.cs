//blockfield
//gamemode by theblackparrot and munkurious

//client.cs

//start the GUI things
exec("./gui.cs");
//DON'T EXECUTE THE Blockfield_HUD.gui I'M JUST USING THAT FOR COPY-PASTE CONVIENENCE; IT'LL ALL BE IN client.cs EVENTUALLY.

//for now
$inBlockfield = 1;


//sending info!
//still need this for highscores!
function clientcmdblockfield_recieveHighscore(%score)
{
	echo("client score is: " @ %score);
}

//decided it would be better to do score client-sided and just have it send the score upon death.
//highscores should be server-sided to prevent changing it illegally. not illegally illegally but- oh whatever.
function clientcmdBlockfield_onDeath()
{

	//figured this would work since this we only need the score on death.
	commandToServer('blockfield_receiveScore',$Blockfield::Score);
	for(%i=0;%i<4;%i++)
	{
	
		cancel($Blockfield_AddScore);
		cancel($Blockfield_Time);
		
	}
	//i like being absolutely positively sure things are canceled don't judge meeee
	//if it's not needed just remove it
	
}

function clientcmdBlockfield_onSpawn()
{

	Blockfield_Score();
	Blockfield_Time();
	
}


//movement
package blockfield_movement
{
	function moveLeft(%this)
	{
		parent::moveLeft(%this);
		if(%this && $inBlockfield)
		{
			commandToServer('blockfield_moveLeft');
		}
	}

	function moveRight(%this)
	{
		parent::moveRight(%this);
		if(%this && $inBlockfield)
		{
			commandToServer('blockfield_moveRight');
		}
	}

	function moveBackward(%this)
	{
		parent::moveBackward(%this);
		if(%this && $inBlockfield)
		{
			commandToServer('blockfield_moveBack');
		}
	}
};
activatePackage(blockfield_movement);



//ALL OF THIS WORKED I TESTED IT MULTIPLE TIMES.