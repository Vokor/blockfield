//blockfield
//gamemode by theblackparrot and munkurious

//server.cs


//no custom games!!!
if( $GameModeArg !$= "Add-Ons/GameMode_Blockfield/gamemode.txt" )
{
	error( "Error: GameMode_Blockfield cannot be used in custom games" );
	return;
}



//exec scripts
exec("./codes/Player_Blockfield.cs");


//still need this for highscores!
//sending info!
function serverCmdblockfield_sendHighscore(%this, %score)
{
	if(isObject(%this))
	{
		commandToClient(%this,'blockfield_recieveScore',%score);
	}
}

//decided it would be better to do score client-sided and just have it send the score upon death.
//highscores should be server-sided to prevent changing it illegally. not illegally illegally but- oh whatever.
function serverCmdblockfield_receiveScore(%this,%score)
{

	echo("Received client score:" SPC %this SPC %score);
	
}



//genstuff testing
//needs to be optimized more but works pretty well so far
function blockfield_generateBlocks(%this)
{
	if(isObject(%this.player))
	{
		//how many blocks can we have for this client?
		%this.blockfield_totalBlocks = 800;


		//create bricks around you or check if too far away and delete
		for(%i = 0; %i < %this.blockfield_totalBlocks; %i++)
		{
			if(isObject(%this.newBlock[%i]))
			{
				if(vectorDist(%this.newBlock[%i].getposition(), %this.player.getPosition()) > 175)
				{
					%this.newBlock[%i].delete();
				}
				else
				{
					//do nothing for now
				}
			}
			else
			{

				%g = 0;
				while(!%g)
				{
					%pos1 = getRandom(-200,200) + getWord(%this.player.getPosition(), 0);
					%pos2 = getRandom(-200,200) + getWord(%this.player.getPosition(), 1);
					%position = %pos1 SPC %pos2 SPC "1";
					if(vectorDist(%position,%this.player.getPosition()) > 140)
					{
						%g = 1;
					}
				}
				//%pos1 = getRandom(-150,150) + getWord(%this.player.getPosition(), 0);
				//%pos2 = getRandom(-150,150) + getWord(%this.player.getPosition(), 1);
				//%position = %pos1 SPC %pos2 SPC "1";
				%rotation = "0 0 0 0";
				%angleID = "0";

				//colors for the levelsss
				switch(%this.blockfield_level)
				{

					case 1:
						%colorID = getRandom(0,2);

					case 2:
						%colorID = getRandom(3,5);

					case 3:
						%colorID = getRandom(6,8);

					case 4:
						%colorID = getRandom(9,11);

					case 5:
						%colorID = getRandom(12,14);

					case 6:
						%colorID = getRandom(15,17);

					default:
						%colorID = getRandom(0,18);
				}

				%colorFXID = "0";
				%shapeFXID = "0";
				%printID = "0";
				%isBasePlate = 1;

				%this.newBlock[%i] = new fxDTSBrick()
				{
					position = %position;
					rotation = %rot;
					angleID = %angleID;
					colorID = %colorID;
					colorFXID = %colorFXID;
					shapeFXID = %shapeFXID;
					printID = %printID;
					dataBlock = brick4xCubeData;
					isBasePlate = %isBasePlate;
					isPlanted = 1;
					scale = "1 1 1";
					client = %this;
            	};

            	%err = %this.newBlock[%i].plant();
            	if(%err != 0)
            	{
            		%this.newBlock[%i].delete();
            	}
			}
		}

		%this.blockfield_generateBlockSchedule = schedule(1000, 0, blockfield_generateBlocks, %this);
	}
}



//movement
function blockfield_movementLoop(%this)
{
	if(isObject(%this.player) && $blockfield_run)
	{
		%this.player.setVelocity($blockfield_speed SPC getWord(%this.player.getvelocity(), 1) SPC "0");

		if(vectorDist(%this.player.getPosition(), %this.blockfield_startPosition) > %this.blockfield_farMark)
		{
			%this.blockfield_level++;
			if(%this.blockfield_level >= 7)
			{
				%this.blockfield_level = 1;
			}

			blockfield_loadEnviorment(%this.blockfield_level);

			%this.blockfield_levelR++;

			%font = "<font:impact:50>";
			%color = "<color:FFFF00>";	
		
			%text = %font @ %color @ "LEVEL UP! Level: " @ %this.blockfield_levelR;
		
			%this.bottomPrint(%text,0,1);

			%this.blockfield_farMark = %this.blockfield_farMark * 2;

			%this.player.setTransform(%this.blockfield_startPosition SPC getWords(%player.getTransform(),3,6));
			$blockfield_speed = $blockfield_speed * 2;
		}

		cancel(%this.player.blockfield_moveLoop);
		%this.player.blockfield_moveLoop = schedule(500,0,blockfield_movementLoop,%this);
	}
}

function serverCmdblockfield_moveLeft(%this)
{
	if(isObject(%this.player) && $blockfield_run)
	{
		if(%this.player.movingRight)
		{
			%this.player.setVelocity($blockfield_speed SPC "0" SPC "0");
			%this.player.movingRight = 0;
			serverCmdblockfield_moveLeft(%this);
		}
		else if(%this.player.movingLeft)
		{
			//do nothing
		}
		else
		{
			%this.player.setVelocity($blockfield_speed SPC $blockfield_speed SPC "0");
			%this.player.movingLeft = 1;
		}
	}
}

function serverCmdblockfield_moveRight(%this)
{
	if(isObject(%this.player) && $blockfield_run)
	{
		if(%this.player.movingLeft)
		{
			%this.player.setVelocity($blockfield_speed SPC "0" SPC "0");
			%this.player.movingLeft = 0;
			serverCmdblockfield_moveRight(%this);
		}
		else if(%this.player.movingRight)
		{
			//do nothing
		}
		else
		{
			%this.player.setVelocity($blockfield_speed SPC "-" @ $blockfield_speed SPC "0");
			%this.player.movingRight = 1;
		}
	}
}

function serverCmdblockfield_moveBack(%this)
{
	if(isObject(%this.player) && $blockfield_run)
	{
		%this.player.setVelocity($blockfield_speed SPC "0" SPC "0");
		%this.player.movingLeft = 0;
		%this.player.movingRight = 0;
	}
}


//game starting stuff
package blockfield_start
{
	function GameConnection::onClientEnterGame(%this)
	{
		parent::onClientEnterGame(%this);
		%this.blockfield_startPosition = %this.player.getPosition();
		%this.blockfield_startStage = 1;
		%this.blockfield_startGame(7);
		blockfield_loadEnviorment(1);
		commandToClient(%this,'Blockfield_InitSys');
	}
	
};
activatePackage(blockfield_start);

function GameConnection::blockfield_startGame(%this, %seconds)
{
	if(%seconds <= 0)
	{
		%this.blockfield_startStage = 2;
	}

	if(%this.blockfield_startStage == 1)
	{

		%font = "<font:impact:50>";
		%color = "<color:FFFF00>";	
		
		%text = %font @ %color @ "<just:center>Game starting in" SPC %seconds SPC "seconds.";
		
		%this.bottomPrint(%text,0,1);

		%this.blockfield_startStageClock = %this.schedule(1000, blockfield_startGame, %seconds--);
	}
	else if(%this.blockfield_startStage == 2)
	{
		%font = "<font:impact:50>";
		%color = "<color:FFFF00>";	
		
		%text = %font @ %color @ "<just:center>GO!";
		
		%this.bottomPrint(%text,0,1);

		%this.blockfield_level = 1;
		%this.blockfield_levelR = 1;
		%this.blockfield_farMark = 1000;
		$blockfield_speed = "30";
		$blockfield_run = 1;
		blockfield_movementLoop(%this);
		blockfield_generateBlocks(%this);
		
		commandToClient(%this,'Blockfield_Start');
	}
}

//load enviorements for levels
function blockfield_loadEnviorment(%number)
{
	//from speedkart gamemode
	//load environment if it exists
	%envFile = "Add-Ons/GameMode_Blockfield/envLevel" @ %number @ ".txt"; 
	if(isFile(%envFile))
	{  
		//echo("parsing env file " @ %envFile);
		//usage: GameModeGuiServer::ParseGameModeFile(%filename, %append);
		//if %append == 0, all minigame variables will be cleared 
		%res = GameModeGuiServer::ParseGameModeFile(%envFile, 1);

		EnvGuiServer::getIdxFromFilenames();
		EnvGuiServer::SetSimpleMode();

		if(!$EnvGuiServer::SimpleMode)     
		{
			EnvGuiServer::fillAdvancedVarsFromSimple();
			EnvGuiServer::SetAdvancedMode();
		}
	}
}

//GUI things, sending stuff.
package blockfield_GUI
{

	function GameConnection::onDeath(%client, %killerPlayer, %killer, %damageType, %damageLoc)
	{
	
		if(isObject(%client))
		{
			//clever workaround eh? :P
			commandToClient(%client,'blockfield_onDeath');
			
		}
		
		parent::onDeath(%client, %killerPlayer, %killer, %damageType, %damageLoc);
		
	}
	
	function GameConnection::onClientLeaveGame(%client)
	{
	
		commandToClient(%client,'Blockfield_onLeaveClient');
		
		parent::onClientLeaveGame(%client);
		
		//doesn't appear to do that command when quitting a single player game?
		
	}
	
	//function GameConnection::spawnPlayer(%client)
	//{
	//
	//	parent::spawnPlayer(%client);
	//	
	//}
	
};
activatePackage(blockfield_GUI);